package com.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.model.PersonModel;
import com.service.PersonService;

@RestController
@RequestMapping("/persons")
public class PersonController {

	@Autowired
	PersonService personService;

	@RequestMapping("/all")
	public HashMap<String, PersonModel> getAll() {
		return personService.getAll();
	}

	@RequestMapping("{id}")
	public PersonModel getPerson(@PathVariable("id") String id){
		return personService.getPerson(id);
	}

}
