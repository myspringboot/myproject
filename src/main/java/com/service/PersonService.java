package com.service;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.model.PersonModel;

@Service
public class PersonService {
	private HashMap<String, PersonModel> persons = null;

	public PersonService() {
		persons = new HashMap<String, PersonModel>();
		persons.put("1", new PersonModel("1", "person 1"));
		persons.put("2", new PersonModel("2", "person 2"));
		persons.put("3", new PersonModel("3", "person 3"));
		persons.put("4", new PersonModel("4", "person 4"));
		persons.put("5", new PersonModel("5", "person 5"));
		persons.put("6", new PersonModel("6", "person 6"));
	}

	public HashMap<String, PersonModel> getAll() {
		return persons;
	}

	public PersonModel getPerson(String id) {
		return persons.get(id);
	}

}
