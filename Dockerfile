FROM openjdk:8-alpine
EXPOSE 8180
ADD target/demoApp.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]